// This file is created by egg-ts-helper@1.30.3
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportComponent = require('../../../app/model/component');
import ExportQuestion = require('../../../app/model/question');
import ExportUser = require('../../../app/model/user');

declare module 'egg' {
  interface IModel {
    Component: ReturnType<typeof ExportComponent>;
    Question: ReturnType<typeof ExportQuestion>;
    User: ReturnType<typeof ExportUser>;
  }
}
