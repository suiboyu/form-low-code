"use strict";
const BaseService = require("./base");

class QuestionService extends BaseService {
  async getList() {
    return this.run(async () => {
      const { ctx, app } = this;
      const result = await ctx.model.Question.findAll();
      return result;
    });
  }

  async add(params) {
    return this.run(async () => {
      const ctx = this;
      const result = await ctx.model.Question.create(params);
      return result;
    })
  }
}

module.exports = QuestionService;
