"use strict";
const BaseService = require("./base");

class ComponentService extends BaseService {
  async add(params) {
    return this.run(async () => {
      const ctx = this;
      const result = await ctx.model.Component.create(params);
      return result;
    })
  }
}

module.exports = ComponentService;
