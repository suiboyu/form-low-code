module.exports = app => {
  const { STRING, INTEGER, TEXT, DATE } = app.Sequelize;

  const User = app.model.define('user', {
    id: { type: INTEGER, primaryKey: true, autoIncrement: true },
    username: STRING(20),
    password: STRING(64),
    rea_name: STRING(20),
    avatar: TEXT('long'),
    phone: STRING(20),
    email: STRING(20),
    gender: STRING(20),
    school: STRING(20),
    academy: STRING(20),
    class: STRING(20),
    sign: STRING(300),
    createTime: DATE,
    updateTime: DATE
  });

  return User;
}