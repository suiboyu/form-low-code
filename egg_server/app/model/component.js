module.exports = app => {
  const { STRING, INTEGER, TEXT, BOOLEAN } = app.Sequelize;

  const Component = app.model.define('component', {
    id: { type: INTEGER, primaryKey: true, autoIncrement: true },
    questionId: INTEGER,
    type: STRING(20),
    title: STRING(64),
    isHidden: BOOLEAN,
    isLocked: BOOLEAN,
    props: TEXT('long')
  });

  return Component;
}