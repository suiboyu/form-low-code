module.exports = app => {
  const { STRING, INTEGER, TEXT, DATE, BOOLEAN } = app.Sequelize;

  const Question = app.model.define('question', {
    id: { type: INTEGER, primaryKey: true, autoIncrement: true },
    title: STRING(20),
    desc: STRING(64),
    js: TEXT('long'),
    css: TEXT('long'),
    phone: STRING(20),
    watermark: BOOLEAN,
    isDeleted: BOOLEAN,
    isPublished: BOOLEAN,
    createTime: DATE,
    updateTime: DATE,
    deleteTime: DATE
  });

  return Question;
}