"use strict";
const BaseController = require("./base");

class QuestionController extends BaseController {
  async getQuestionList() {
    const { ctx, app } = this;
    const questionList = await ctx.service.question.getList()
    this.success({
      code: 200,
      data: questionList
    })
  }

  async createQuestion() {
    const { ctx, app } = this;
    const parmas = ctx.params();
    const questionId = await ctx.service.question.add({
      ...parmas
    })
    const result = await ctx.service.component.add({

    })
    this.success({
      code: 200,
      data: questionList
    })
  }
}

module.exports = QuestionController;