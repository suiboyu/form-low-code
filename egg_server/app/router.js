'use strict';

module.exports = app => {
  const { router, controller } = app;
  const userExist = app.middleware.userExist();
  router.post('/api/user/register', controller.user.register);
  router.post('/api/user/login', controller.user.login);
  router.post('/api/user/logout', userExist, controller.user.logout);
  router.get('/api/question/list', controller.question.getQuestionList);
};
