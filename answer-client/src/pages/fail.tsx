import React, { useEffect } from "react";
import PageWrapper from "@/components/PageWrapper";
import { ErrorBlock } from "antd-mobile";

export default function Fail() {
  useEffect(() => {
    document.body.style.background = "var(--adm-color-background)";
  }, []);

  return (
    <PageWrapper title="提交失败">
      <ErrorBlock fullPage />
    </PageWrapper>
  );
}
