import { Result } from "antd-mobile";
import PageWrapper from "@/components/PageWrapper";

export default function Success() {
  return (
    <PageWrapper title="提交成功">
      <Result status="success" title="操作成功" />
    </PageWrapper>
  );
}
