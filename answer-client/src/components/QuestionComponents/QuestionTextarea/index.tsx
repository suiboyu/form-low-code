import React, { FC } from "react";
import { TextArea } from "antd-mobile";

type PropsType = {
  fe_id: string;
  props: {
    title: string;
    placeholder?: string;
  };
};

const QuestionTextarea: FC<PropsType> = ({ props }) => {
  const { title, placeholder = "" } = props;

  return (
    <>
      <h3>{title}</h3>
      <TextArea placeholder={placeholder} rows={5}></TextArea>
    </>
  );
};

export default QuestionTextarea;
