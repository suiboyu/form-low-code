import React, { FC } from "react";
import { Radio, Space } from "antd-mobile";

type PropsType = {
  fe_id: string;
  props: {
    title: string;
    options: Array<{
      value: string;
      text: string;
    }>;
    value: string;
    isVertical: boolean;
  };
};

const QuestionRadio: FC<PropsType> = ({ props }) => {
  const { title, options = [], value, isVertical } = props;

  return (
    <>
      <h3>{title}</h3>
      <Radio.Group defaultValue={value}>
        <Space direction={isVertical ? "vertical" : "horizontal"}>
          {options.map((opt) => {
            const { value, text } = opt;
            return (
              <Radio key={value} value={value}>
                {text}
              </Radio>
            );
          })}
        </Space>
      </Radio.Group>
    </>
  );
};

export default QuestionRadio;
