import React, { FC } from "react";
import { Input } from 'antd-mobile';

type PropsType = {
  fe_id: string;
  props: {
    title: string;
    placeholder?: string;
  };
};

const QuestionInput: FC<PropsType> = ({ props }) => {
  const { title, placeholder = "" } = props;

  return (
    <>
      <h3>{title}</h3>
      <Input placeholder={placeholder}></Input>
    </>
  );
};

export default QuestionInput;
