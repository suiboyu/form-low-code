import React, { FC, CSSProperties } from "react";
import { Rate } from 'antd-mobile'

type PropsType = {
  title: string;
  count: number;
  isCenter?: boolean;
};

const QuestionTitle: FC<PropsType> = (props) => {
  const { title, count } = props;
  return (
    <>
      <h3>{title}</h3>
      <Rate count={count} />
    </>
  )
};

export default QuestionTitle;
