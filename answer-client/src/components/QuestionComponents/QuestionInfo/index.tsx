import React, { FC } from "react";

type PropsType = {
  title: string;
  desc?: string;
};

const QuestionInfo: FC<PropsType> = (props) => {
  const { title, desc = "" } = props;

  const descTextList = desc.split("\n");

  return (
    <div style={{ textAlign: "center" }}>
      <p style={{ fontSize: "24px" }}>{title}</p>
      <p>
        {descTextList.map((t, index) => (
          <span key={index}>
            {index > 0 && <br />}
            {t}
          </span>
        ))}
      </p>
    </div>
  );
};

export default QuestionInfo;
