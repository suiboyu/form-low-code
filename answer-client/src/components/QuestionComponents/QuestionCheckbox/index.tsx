import React, { FC, useState } from "react";
import { Checkbox, Space } from "antd-mobile";

type PropsType = {
  fe_id: string;
  props: {
    title: string;
    isVertical?: boolean;
    list: Array<{
      value: string;
      text: string;
      checked: boolean;
    }>;
  };
};

const QuestionCheckbox: FC<PropsType> = ({ props }) => {
  const { title, isVertical, list = [] } = props;
  const [value, setValue] = useState<string[]>([]);

  return (
    <>
      <h3>{title}</h3>
      <Checkbox.Group
        value={value}
        onChange={(val) => {
          setValue(val as string[]);
        }}
      >
        <Space direction={isVertical ? "vertical" : "horizontal"}>
          {list.map((opt) => {
            const { value, text, checked } = opt;
            return (
              <Checkbox key={value} value={value} checked={checked}>
                {text}
              </Checkbox>
            );
          })}
        </Space>
      </Checkbox.Group>
    </>
  );
};

export default QuestionCheckbox;
