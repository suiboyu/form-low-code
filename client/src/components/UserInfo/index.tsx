import React, { FC } from 'react'
import { Button, message } from 'antd'
import { Link, useNavigate } from 'react-router-dom'
import { UserOutlined } from '@ant-design/icons'
import { useDispatch } from 'react-redux'
import useGetUserInfo from '@/hooks/useGetUserInfo'
import { logoutReducer } from '@/store/userReducer'
import { LOGIN_PATHNAME } from '@/utils/constant'
import { removeToken } from '@/utils/utils'

const UserInfo: FC = () => {
  const nav = useNavigate()
  const dispatch = useDispatch()
  const { username, nickname } = useGetUserInfo()

  const logout = () => {
    dispatch(logoutReducer())
    removeToken()
    message.success('退出成功')
    nav(LOGIN_PATHNAME)
  }

  return (
    <div>
      {username ? (
        <>
          <span style={{ color: '#e8e8e8' }}>
            <UserOutlined />
            {nickname}
          </span>
          <Button type="link" onClick={logout}>
            退出
          </Button>
        </>
      ) : (
        <Link to={LOGIN_PATHNAME}>登录</Link>
      )}
    </div>
  )
}

export default UserInfo
