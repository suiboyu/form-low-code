export type QuestionRatePropsType = {
  title?: string
  count?: number
  onChange?: (newProps: QuestionRatePropsType) => void
  disabled?: boolean
}

export const QuestionRateDefaultProps: QuestionRatePropsType = {
  title: '问卷评分',
  count: 5,
}
