import { FC } from 'react'
import { Typography, Rate } from 'antd'
import { QuestionRatePropsType, QuestionRateDefaultProps } from './interface'

const { Paragraph } = Typography

const Component: FC<QuestionRatePropsType> = (props: QuestionRatePropsType) => {
  const { title, count } = { ...QuestionRateDefaultProps, ...props }

  return (
    <>
      <Paragraph strong>{title}</Paragraph>
      <Rate count={count} />
    </>
  )
}

export default Component
