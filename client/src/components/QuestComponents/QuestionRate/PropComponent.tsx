import React, { FC, useEffect } from 'react'
import { Form, Input, InputNumber } from 'antd'
import { QuestionRatePropsType } from './interface'

const PropComponent: FC<QuestionRatePropsType> = (props: QuestionRatePropsType) => {
  const { title, count, onChange, disabled } = props
  const [form] = Form.useForm()

  useEffect(() => {
    form.setFieldsValue({ title, count })
  }, [title, count, form])

  function handleValuesChange() {
    if (onChange) {
      onChange(form.getFieldsValue())
    }
  }

  return (
    <Form
      layout="vertical"
      initialValues={{ title, count }}
      form={form}
      onValuesChange={handleValuesChange}
      disabled={disabled}
    >
      <Form.Item label="标题" name="title" rules={[{ required: true, message: '请输入问卷标题' }]}>
        <Input />
      </Form.Item>
      <Form.Item label="数量" name="count" rules={[{ required: true, message: '请输入数量' }]}>
        <InputNumber min={5} max={10} />
      </Form.Item>
    </Form>
  )
}

export default PropComponent
