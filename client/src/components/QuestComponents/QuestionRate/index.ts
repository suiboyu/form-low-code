import Component from './Component'
import PropComponent from './PropComponent'
import { QuestionRateDefaultProps } from './interface'

export * from './interface'

const componentData = {
  title: '问卷评分',
  type: 'questionRate',
  Component,
  PropComponent,
  defaultProps: QuestionRateDefaultProps,
}
export default componentData
