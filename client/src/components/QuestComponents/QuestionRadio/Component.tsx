import { FC } from 'react'
import { Typography, Space, Radio } from 'antd'
import { QuestionRadioDefaultProps, QuestionRadioPropsType } from './interface'

const { Paragraph } = Typography

const Component: FC<QuestionRadioPropsType> = (props: QuestionRadioPropsType) => {
  const {
    title,
    isVertical,
    options = [],
  } = {
    ...QuestionRadioDefaultProps,
    ...props,
  }

  return (
    <>
      <Paragraph strong>{title}</Paragraph>
      <Space direction={isVertical ? 'vertical' : 'horizontal'}>
        {options.map(opt => {
          const { value, text } = opt
          return (
            <Radio key={value} value={value}>
              {text}
            </Radio>
          )
        })}
      </Space>
    </>
  )
}

export default Component
