import Component from './Component'
import PropComponent from './PropComponent'
import { QuestionInfoDefaultProps } from './interface'

export * from './interface'

const componentData = {
  title: '问卷表格',
  type: 'questionTable',
  Component,
  PropComponent,
  defaultProps: QuestionInfoDefaultProps,
}
export default componentData
