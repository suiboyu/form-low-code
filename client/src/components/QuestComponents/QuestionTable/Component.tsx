import { FC } from 'react'
import { Typography, Table } from 'antd'
import { QuestionTablePropsType, QuestionInfoDefaultProps } from './interface'

const { Paragraph } = Typography

const Component: FC<QuestionTablePropsType> = (props: QuestionTablePropsType) => {
  const { title, dataSource, columns } = {
    ...QuestionInfoDefaultProps,
    ...props,
  }

  return (
    <div>
      <Paragraph strong>{title}</Paragraph>
      <Table dataSource={dataSource} columns={columns} pagination={false} />
    </div>
  )
}

export default Component
