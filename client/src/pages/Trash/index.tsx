import React, { FC, useState } from 'react'
import {
  Typography,
  Empty,
  Space,
  Tag,
  Table,
  Button,
  Divider,
  Spin,
  Popconfirm,
  message,
} from 'antd'
import { useRequest } from 'ahooks'
import ListSearch from '@/components/ListSearch'
import ListPagination from '@/components/ListPagination'
import useLoadQuestionListData from '@/hooks/useLoadQuestionListData'
import { updateQuestionService, deleteQuestionsService } from '@/api/question'
import styles from '@/styles/common.module.scss'

const tableColumns = [
  {
    title: '标题',
    dataIndex: 'title',
  },
  {
    title: '是否发布',
    dataIndex: 'isPublished',
    render: (isPublished: boolean) => {
      return isPublished ? <Tag color="processing">已发布</Tag> : <Tag>未发布</Tag>
    },
  },
  {
    title: '答卷',
    dataIndex: 'answerCount',
  },
  {
    title: '创建时间',
    dataIndex: 'createdAt',
  },
]

const { Title } = Typography

const Star: FC = () => {
  const [selectedIds, setSelectedIds] = useState([])
  const {
    data = {},
    loading,
    refresh,
  } = useLoadQuestionListData({
    isDeleted: true,
  })
  const { list = [], total = 0 } = data

  const { run: recover } = useRequest(
    async () => {
      for await (const id of selectedIds) {
        await updateQuestionService(id, { isDeleted: false })
      }
    },
    {
      manual: true,
      debounceWait: 500,
      onSuccess() {
        message.success('恢复成功')
        refresh()
        setSelectedIds([])
      },
    }
  )

  const { run: deleteQuestion } = useRequest(
    async () => await deleteQuestionsService(selectedIds),
    {
      manual: true,
      onSuccess() {
        message.success('删除成功')
        refresh()
        setSelectedIds([])
      },
    }
  )

  return (
    <>
      <div className={styles.header}>
        <div className={styles.left}>
          <Title level={3}>回收站</Title>
        </div>
        <div className={styles.right}>
          <ListSearch />
        </div>
      </div>
      <div className={styles.content}>
        {loading && (
          <div style={{ textAlign: 'center' }}>
            <Spin />
          </div>
        )}
        {!loading && list.length === 0 && <Empty description="暂无数据" />}
        {!loading && list.length > 0 && (
          <>
            <Space>
              <Button type="primary" disabled={selectedIds.length === 0} onClick={recover}>
                恢复
              </Button>
              <Popconfirm
                title="确定复制该问卷？"
                okText="确定"
                cancelText="取消"
                onConfirm={deleteQuestion}
              >
                <Button danger disabled={selectedIds.length === 0} onClick={deleteQuestion}>
                  彻底删除
                </Button>
              </Popconfirm>
            </Space>
            <Divider style={{ margin: '12px 0' }} />
            <Table
              columns={tableColumns}
              dataSource={list}
              pagination={false}
              rowKey={q => q._id}
              rowSelection={{
                type: 'checkbox',
                onChange: (selectedRowKeys: any) => {
                  setSelectedIds(selectedRowKeys)
                },
              }}
            />
          </>
        )}
      </div>
      <div className={styles.footer}>
        <ListPagination total={total} />
      </div>
    </>
  )
}

export default Star
