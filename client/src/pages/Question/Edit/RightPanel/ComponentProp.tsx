import { FC } from 'react'
import useGetComponentInfo from '@/hooks/useGetComponentInfo'
import { ComponentPropsType, getComponentConfByType } from '@/components/QuestComponents'
import { changeComponentProps } from '@/store/componentsReducer'
import { useDispatch } from 'react-redux'

const NoProp: FC = () => {
  return <div style={{ textAlign: 'center' }}>未选中组件</div>
}

const ComponentProp: FC = () => {
  const dispatch = useDispatch()
  const { selectComponent } = useGetComponentInfo()

  if (selectComponent == null) return <NoProp />

  const { type, props, isLocked, isHidden } = selectComponent
  const componentConf = getComponentConfByType(type)

  if (componentConf == null) return <NoProp />

  const { PropComponent } = componentConf

  const changeProps = (newProps: ComponentPropsType) => {
    if (selectComponent == null) return
    const { fe_id } = selectComponent
    dispatch(changeComponentProps({ fe_id, newProps }))
  }

  return <PropComponent {...props} onChange={changeProps} disabled={isLocked || isHidden} />
}

export default ComponentProp
