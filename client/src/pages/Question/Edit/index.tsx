import React, { FC } from 'react'
import { useDispatch } from 'react-redux'
import { Watermark } from 'antd'
import useLoadQuestionData from '@/hooks/useLoadQuestionData'
import { changeSelectedId } from '@/store/componentsReducer'
import useGetPageInfo from '@/hooks/useGetPageInfo'
import EditCanvas from './EditCanvas'
import EditHeader from './EditHeader'
import LeftPanel from './LeftPanel'
import RightPanel from './RightPanel'
import styles from './index.module.scss'

const Edit: FC = () => {
  const { loading } = useLoadQuestionData()
  const { watermark } = useGetPageInfo()
  const dispatch = useDispatch()

  const clearSelectedId = () => {
    dispatch(changeSelectedId(''))
  }

  return (
    <div className={styles.container}>
      <EditHeader />
      <div className={styles['content-wrapper']}>
        <div className={styles.content}>
          <div className={styles.left}>
            <LeftPanel />
          </div>
          <div className={styles.main} onClick={clearSelectedId}>
            <div className={styles['canvas-wrapper']}>
              {watermark ? (
                <Watermark content="版权所属">
                  <EditCanvas loading={loading} />
                </Watermark>
              ) : (
                <EditCanvas loading={loading} />
              )}
            </div>
          </div>
          <div className={styles.right}>
            <RightPanel />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Edit
