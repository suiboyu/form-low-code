import React, { FC } from 'react'
import { Typography, Empty, Spin } from 'antd'
import ListSearch from '@/components/ListSearch'
import QuestionCard from '@/components/QuestionCard'
import useLoadQuestionListData from '@/hooks/useLoadQuestionListData'
import ListPagination from '@/components/ListPagination'
import styles from '@/styles/common.module.scss'

const { Title } = Typography

const Star: FC = () => {
  const { data = {}, loading } = useLoadQuestionListData({
    isStar: true,
  })
  const { list = [], total = 0 } = data

  return (
    <>
      <div className={styles.header}>
        <div className={styles.left}>
          <Title level={3}>星标问卷</Title>
        </div>
        <div className={styles.right}>
          <ListSearch />
        </div>
      </div>
      <div className={styles.content}>
        {loading && (
          <div style={{ textAlign: 'center' }}>
            <Spin />
          </div>
        )}
        {!loading && list.length === 0 && <Empty description="暂无数据" />}
        {!loading &&
          list.length > 0 &&
          list.map((item: any) => {
            const { _id } = item
            return <QuestionCard key={_id} {...item} />
          })}
      </div>
      <div className={styles.footer}>
        <ListPagination total={total} />
      </div>
    </>
  )
}

export default Star
