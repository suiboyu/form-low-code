import { useSelector } from 'react-redux'
import { StateType } from '@/store'
import { ComponentsStateType } from '@/store/componentsReducer'

function useGetComponentInfo() {
  const components = useSelector<StateType>(
    state => state.components.present
  ) as ComponentsStateType
  const { componentList = [], selectedId, copiedComponent = null } = components

  const selectComponent = componentList.find(c => c.fe_id === selectedId)

  return {
    componentList,
    selectedId,
    selectComponent,
    copiedComponent,
  }
}

export default useGetComponentInfo
